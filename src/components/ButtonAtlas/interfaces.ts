import { ButtonProps } from "@mui/material";

export interface IButtonAtlas extends ButtonProps {
  isSecond?: boolean;
}
