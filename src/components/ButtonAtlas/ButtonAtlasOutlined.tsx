import { Button, styled, useTheme } from "@mui/material";
import { IButtonAtlas } from "src/components/ButtonAtlas/interfaces";

export const ButtonAtlasOutlined = (props: IButtonAtlas) => {
  const { children } = props;
  const theme = useTheme();

  const StyledButton = styled(Button, {
    shouldForwardProp: (prop) => prop !== "isSecond",
  })<IButtonAtlas>(({ isSecond }) => ({
    padding: "13px",
    color: isSecond ? theme.atlasColors.grey400 : theme.atlasColors.primary,
    textTransform: "none",
    width: "290px",
    textAlign: "center",
    backgroundColor: theme.atlasColors.white,
    border: "1px solid",
    borderColor: isSecond
      ? theme.atlasColors.grey400
      : theme.atlasColors.primary,
    "&:hover": {
      borderColor: isSecond
        ? theme.atlasColors.grey500
        : theme.atlasColors.second,
      color: isSecond ? theme.atlasColors.grey500 : theme.atlasColors.second,
      backgroundColor: theme.atlasColors.white,
    },
    "&:disabled": {
      opacity: 0.3,
      color: isSecond ? theme.atlasColors.grey500 : theme.atlasColors.primary,
    },
  }));

  return <StyledButton {...props}>{children}</StyledButton>;
};
