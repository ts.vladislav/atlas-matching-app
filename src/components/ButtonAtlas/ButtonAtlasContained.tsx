import { Button, styled, useTheme } from "@mui/material";
import { IButtonAtlas } from "src/components/ButtonAtlas/interfaces";

export const ButtonAtlasContained = (props: IButtonAtlas) => {
  const { children } = props;
  const theme = useTheme();

  const StyledButton = styled(Button, {
    shouldForwardProp: (prop) => prop !== "isSecond",
  })<IButtonAtlas>(({ isSecond }) => ({
    padding: "13px",
    color: theme.atlasColors.white,
    textTransform: "none",
    width: "290px",
    textAlign: "center",
    backgroundColor: isSecond
      ? theme.atlasColors.grey400
      : theme.atlasColors.primary,
    "&:hover": {
      backgroundColor: isSecond
        ? theme.atlasColors.grey500
        : theme.atlasColors.second,
    },
    "&:disabled": {
      opacity: 0.3,
      color: isSecond ? theme.atlasColors.white : theme.atlasColors.white,
      backgoundColor: theme.atlasColors.white,
    },
  }));

  return <StyledButton {...props}>{children}</StyledButton>;
};
