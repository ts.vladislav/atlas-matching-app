import { IStopItem } from "src/store/handbook/interfaces";

export interface IInputAtlasBorderless {
  value: string | number;
  valueType: keyof IStopItem;
  setValue: (
    prop: keyof IStopItem,
  ) => (event: React.ChangeEvent<HTMLInputElement>) => void;
  type: string;
  label: string;
  resetValue: (prop: keyof IStopItem) => void;
  placeholder?: string;
}
