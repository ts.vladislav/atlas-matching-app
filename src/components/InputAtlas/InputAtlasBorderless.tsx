import React from "react";

import { Typography, useTheme } from "@mui/material";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";
import CloseIcon from "@mui/icons-material/Close";
import classNames from "classnames/bind";

import styles from "./styles.module.scss";
import { IInputAtlasBorderless } from "./interfaces";

export const InputAtlasBorderless = (props: IInputAtlasBorderless) => {
  const cn = classNames.bind(styles);
  const theme = useTheme();
  const { value, valueType, setValue, type, label, resetValue, placeholder } =
    props;

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault();
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Typography
        variant="subtitle2"
        sx={{ color: theme.atlasColors.grey500, mb: 0.5, fontWeight: 400 }}
      >
        {label}
      </Typography>
      <Box className={cn(["input-wrap"])}>
        <input
          type={type}
          className={cn(["input"])}
          value={value}
          onChange={setValue(valueType)}
          placeholder={placeholder}
          min="0"
        />
        <IconButton
          onClick={() => resetValue(valueType)}
          onMouseDown={handleMouseDownPassword}
          edge="end"
        >
          <Box
            sx={{
              bgcolor: theme.atlasColors.grey400,
              borderRadius: "50%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "16px",
              height: "16px",
            }}
          >
            <CloseIcon sx={{ fill: theme.atlasColors.white, width: "12px" }} />
          </Box>
        </IconButton>
      </Box>
    </Box>
  );

  // return (
  //   <FormControl sx={{ mb: 2, width: "100%" }} variant="outlined">
  //     <InputLabel htmlFor="outlined-adornment-password">{label}</InputLabel>
  //     <OutlinedInput
  //       id="outlined-adornment-password"
  //       type={type}
  //       value={value}
  //       onChange={setValue("title")}
  //       endAdornment={
  //         <InputAdornment position="end">
  //           <IconButton
  //             aria-label="toggle password visibility"
  //             onClick={handleClickResetValue}
  //             onMouseDown={handleMouseDownPassword}
  //             edge="end"
  //           >
  //             <Box
  //               sx={{
  //                 bgcolor: theme.atlasColors.grey400,
  //                 borderRadius: "50%",
  //                 display: "flex",
  //                 alignItems: "center",
  //                 justifyContent: "center",
  //                 width: "16px",
  //                 height: "16px",
  //               }}
  //             >
  //               <CloseIcon
  //                 sx={{ fill: theme.atlasColors.white, width: "12px" }}
  //               />
  //             </Box>
  //           </IconButton>
  //         </InputAdornment>
  //       }
  //       label={label}
  //     />
  //   </FormControl>
  // );
};
