import PersonIcon from "@mui/icons-material/Person";
import {
  Container,
  AppBar,
  Toolbar,
  Button,
  useTheme,
  Box,
} from "@mui/material";
import { Link } from "react-router-dom";
import classNames from "classnames/bind";
import styled from "@emotion/styled";

import styles from "./styles.module.scss";

export const Header = () => {
  const cn = classNames.bind(styles);
  const theme = useTheme();

  const HeaderLink = styled(Link, {
    shouldForwardProp: (prop) => prop !== "isActive",
  })<{ isActive?: boolean }>(({ isActive }) => ({
    color: theme.atlasColors.primary,
    fontWeight: isActive ? "bold" : "regular",
    "&:last-child": {
      marginLeft: "24px",
    },
  }));

  return (
    <AppBar
      position="static"
      sx={{
        bgcolor: theme.atlasColors.white,
        maxHeight: { lg: "60px" },
        boxShadow:
          "0px 0px 2px rgba(8, 78, 104, 0.18), 0px 2px 4px rgba(8, 78, 104, 0.12)",
      }}
    >
      <Container>
        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Link to="/">
            <img src="/logo_full_svg.svg" alt="logotype" />
          </Link>
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <HeaderLink to="/handbook">Справочник</HeaderLink>
            <HeaderLink isActive={true} to="/matching">
              Сопоставление
            </HeaderLink>
          </Box>
          <Link to="/">
            <Button
              sx={{ color: theme.atlasColors.grey500, textTransform: "none" }}
              className={cn("profile-button")}
              color="inherit"
            >
              <PersonIcon sx={{ fill: "#68787D" }} />
              Профиль
            </Button>
          </Link>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
