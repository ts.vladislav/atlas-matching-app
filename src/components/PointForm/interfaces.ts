import { IStopItem } from "src/store/handbook/interfaces";

export interface IPointForm {
  values: IStopItem;
  setValues: (values: IStopItem) => void;
  method: "POST" | "PUT" | "PATCH";
  isHorizontal?: boolean;
  title?: string;
}
