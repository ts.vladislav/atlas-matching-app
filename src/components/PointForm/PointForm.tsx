import React from "react";
import Box from "@mui/material/Box";
import { IStopItem } from "src/store/handbook/interfaces";
import { InputAtlasBorderless } from "src/components/InputAtlas";
import { ButtonAtlasContained } from "src/components/ButtonAtlas";
import { IPointForm } from "src/components/PointForm/interfaces";
import { Typography } from "@mui/material";

export const PointForm = (props: IPointForm) => {
  const { values, setValues, isHorizontal, title } = props;
  const method = props.method.toLowerCase();

  const texts: any = {
    post: {
      button: "Добавить",
    },
    put: {
      button: "Найти",
    },
  };
  const handleChange =
    (prop: keyof IStopItem) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setValues({ ...values, [prop]: event.target.value });
    };

  const handleResetValue = (prop: keyof IStopItem) => {
    setValues({ ...values, [prop]: "" });
  };

  if (isHorizontal) {
    return (
      <form>
        <Box
          sx={{
            padding: "24px 0",
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            width: "100%",
          }}
        >
          {title && (
            <Typography sx={{ fontWeight: 500, mb: 2 }}>{title}</Typography>
          )}
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <Box sx={{ width: "100%", mr: 2.5 }}>
              <InputAtlasBorderless
                value={values.title}
                valueType="title"
                setValue={handleChange}
                type="text"
                label="Наименование остановки"
                resetValue={handleResetValue}
              />
            </Box>
            <Box sx={{ width: "100%", mr: 2.5 }}>
              <InputAtlasBorderless
                value={values.latitude}
                valueType="latitude"
                setValue={handleChange}
                type="number"
                label="Широта"
                resetValue={handleResetValue}
              />
            </Box>
            <Box sx={{ width: "100%" }}>
              <InputAtlasBorderless
                value={values.longitude}
                valueType="longitude"
                setValue={handleChange}
                type="number"
                label="Долгота"
                resetValue={handleResetValue}
              />
            </Box>
          </Box>
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <ButtonAtlasContained>{texts[method].button}</ButtonAtlasContained>
          </Box>
        </Box>
      </form>
    );
  }

  return (
    <form>
      <Box sx={{ padding: "16px 24px 24px 24px" }}>
        <InputAtlasBorderless
          value={values.title}
          valueType="title"
          setValue={handleChange}
          type="text"
          label="Наименование остановки"
          resetValue={handleResetValue}
        />
        <InputAtlasBorderless
          value={values.latitude}
          valueType="latitude"
          setValue={handleChange}
          type="number"
          label="Широта"
          resetValue={handleResetValue}
        />
        <InputAtlasBorderless
          value={values.longitude}
          valueType="longitude"
          setValue={handleChange}
          type="number"
          label="Долгота"
          resetValue={handleResetValue}
        />
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <ButtonAtlasContained>{texts[method].button}</ButtonAtlasContained>
        </Box>
      </Box>
    </form>
  );
};
