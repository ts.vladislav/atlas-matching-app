import { IStopItemList } from "src/store/handbook/interfaces";

export interface IMatchingPointInfo {
  title: string;
  data: IStopItemList;
}
