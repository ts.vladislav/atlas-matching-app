import { Box, Typography } from "@mui/material";
import { MatchingPointInfoListItem } from "src/components/_pages/Matching/MatchingPointInfoListItem";

import { IMatchingPointInfo } from "./interfaces";

export const MatchingPointInfo = (props: IMatchingPointInfo) => {
  const { title, data } = props;
  return (
    <Box>
      <Typography sx={{ fontWeight: 700, mb: 2 }}>{title}</Typography>
      <ul>
        {Object.entries(data).map(([title, value]) => (
          <MatchingPointInfoListItem key={title} title={title} value={value} />
        ))}
      </ul>
    </Box>
  );
};
