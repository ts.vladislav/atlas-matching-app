import { Box, Container, Grid, useTheme } from "@mui/material";

import {
  ButtonAtlasContained,
  ButtonAtlasOutlined,
} from "src/components/ButtonAtlas";

export const MatchingStickyBar = () => {
  const theme = useTheme();
  return (
    <Box
      sx={{
        padding: "15px 0",
        bgcolor: theme.atlasColors.white,
        position: "fixed",
        bottom: 0,
        width: "100%",
      }}
    >
      <Container>
        <Grid container spacing={2}>
          <Grid item md={8}>
            <Box>
              <ButtonAtlasOutlined isSecond disabled sx={{ mr: "28px" }}>
                Отменить сопоставление
              </ButtonAtlasOutlined>
              <ButtonAtlasOutlined disabled>Сопоставить</ButtonAtlasOutlined>
            </Box>
          </Grid>
          <Grid item md={4}>
            <Box>
              <ButtonAtlasContained disabled>
                Отменить сопоставление
              </ButtonAtlasContained>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};
