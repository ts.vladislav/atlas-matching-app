export interface IMatchingPointInfoListItem {
  title: string;
  value: string | number;
}
