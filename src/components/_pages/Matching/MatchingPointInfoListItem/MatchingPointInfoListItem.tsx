import classNames from "classnames/bind";

import { IMatchingPointInfoListItem } from "./interfaces";
import styles from "./styles.module.scss";

export const MatchingPointInfoListItem = (
  props: IMatchingPointInfoListItem,
) => {
  const { title, value } = props;
  const cn = classNames.bind(styles);
  const book: any = {
    id: "id",
    title: "Наименование остановки",
    latitude: "Широта",
    longitude: "Долгота",
  };
  return (
    <li className={cn("list-item")}>
      <span className={cn("list-item__title")}>{book[title]}</span>
      <p className={cn("list-item__value")}>{value}</p>
    </li>
  );
};
