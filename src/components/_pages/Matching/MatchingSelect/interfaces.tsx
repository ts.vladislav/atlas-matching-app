export interface IMatchingSelectProps {
  names: string[];
  personName: any;
  handleChangeSelect: (value: any) => void;
}
