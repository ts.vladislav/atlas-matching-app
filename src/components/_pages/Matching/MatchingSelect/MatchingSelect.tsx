import { IMatchingSelectProps } from "src/components/_pages/Matching/MatchingSelect/interfaces";

import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import OutlinedInput from "@mui/material/OutlinedInput";
import { Theme, useTheme } from "@mui/material/styles";
import { Typography } from "@mui/material";

export const MatchingSelect = (props: IMatchingSelectProps) => {
  const { names, personName, handleChangeSelect } = props;
  const theme = useTheme();

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;

  const MenuProps = {
    PaperProps: {
      style: {
        marginTop: "10px",
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  function getStyles(
    name: string,
    personName: readonly string[],
    theme: Theme,
  ) {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
  }

  return (
    <FormControl sx={{ m: 1, width: 300, mt: 3.5 }}>
      <Typography
        sx={{
          color: theme.atlasColors.grey500,
          fontWeight: 500,
          mb: 1,
          fontSize: 14,
        }}
      >
        Партнер
      </Typography>
      <Select
        disabled={!names.length}
        sx={{ bgcolor: theme.atlasColors.white }}
        displayEmpty
        value={personName}
        onChange={handleChangeSelect}
        input={<OutlinedInput />}
        renderValue={(selected) => {
          if (selected.length === 0) {
            return (
              <Typography
                variant="body1"
                sx={{ color: theme.atlasColors.grey400 }}
              >
                {names.length > 0
                  ? "Выберите партнера"
                  : "Список партнеров пуст"}
              </Typography>
            );
          }

          return selected.join(", ");
        }}
        MenuProps={MenuProps}
        inputProps={{ "aria-label": "Without label" }}
      >
        <MenuItem disabled value="">
          <em>Выберите партнера</em>
        </MenuItem>
        {names.length > 0 &&
          names.map((name) => (
            <MenuItem
              key={name}
              value={name}
              style={getStyles(name, personName, theme)}
            >
              {name}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};
