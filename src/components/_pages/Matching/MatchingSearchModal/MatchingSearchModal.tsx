import React from "react";
import { IMatchingSearchModal } from "src/components/_pages/Matching/MatchingSearchModal/interfaces";
import { Container, Grid, IconButton, useTheme } from "@mui/material";
import Box from "@mui/material/Box";
import Dialog from "@mui/material/Dialog";
import CloseIcon from "@mui/icons-material/Close";

import { MatchingPointInfo } from "src/components/_pages/Matching/MatchingPointInfo";
import { MatchingSearchForm } from "src/components/_pages/Matching/MatchingSearchForm";

export const MatchingSearchModal = (props: IMatchingSearchModal) => {
  const { open, setOpen } = props;
  const handleClose = () => setOpen(false);
  const theme = useTheme();

  return (
    <Dialog open={open} onClose={handleClose} maxWidth="lg" fullWidth={true}>
      <Container maxWidth={false} sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item md={12}>
            <Box
              sx={{
                padding: "24px 0 16px 0",
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <IconButton onClick={() => setOpen(false)}>
                <CloseIcon color="primary" />
              </IconButton>
            </Box>
          </Grid>
          <Grid item md={6}>
            <MatchingPointInfo
              title="Остановка партнера"
              data={{
                id: 123,
                title: "Ленина",
                latitude: 54.321312312,
                longitude: 32.13123123,
              }}
            />
          </Grid>
          <Grid item md={6}>
            <MatchingPointInfo
              title="Остановка атласа"
              data={{
                id: 123,
                title: "Ленина",
                latitude: 54.321312312,
                longitude: 32.13123123,
              }}
            />
          </Grid>
        </Grid>
      </Container>
      <Box sx={{ width: "100%", bgcolor: theme.atlasColors.grey100 }}>
        <Container maxWidth={false} sx={{ flexGrow: 1 }}>
          <Grid container spacing={2}>
            <Grid item md={12}>
              <MatchingSearchForm />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Dialog>
  );
};
