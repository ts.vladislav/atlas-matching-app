export interface IMatchingSearchModal {
  open: boolean;
  setOpen: (value: boolean) => void;
}
