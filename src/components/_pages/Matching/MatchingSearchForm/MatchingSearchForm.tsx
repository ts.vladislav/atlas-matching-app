import React from "react";
import { PointForm } from "src/components/PointForm";
import { IStopItem } from "src/store/handbook/interfaces";

export const MatchingSearchForm = () => {
  const [values, setValues] = React.useState<IStopItem>({
    title: "",
    latitude: 0,
    longitude: 0,
  });
  return (
    <PointForm
      title="Поиск по остановкам Атласа"
      isHorizontal
      method="PUT"
      values={values}
      setValues={setValues}
    />
  );
};
