export interface ITableModalAddNewPoint {
  open: boolean;
  setOpen: (value: boolean) => void;
}
