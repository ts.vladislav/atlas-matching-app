import React from "react";

import { IconButton } from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Dialog from "@mui/material/Dialog";
import CloseIcon from "@mui/icons-material/Close";
import { PointForm } from "src/components/PointForm";
import { IStopItem } from "src/store/handbook/interfaces";

import { ITableModalAddNewPoint } from "./interfaces";

export const TableModalAddNewPoint = (props: ITableModalAddNewPoint) => {
  const { open, setOpen } = props;
  const handleClose = () => setOpen(false);
  const [values, setValues] = React.useState<IStopItem>({
    title: "",
    latitude: 0,
    longitude: 0,
  });

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={{ width: "600px" }}>
        <Box
          sx={{
            padding: "24px 24px 16px 24px",
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <IconButton onClick={() => setOpen(false)}>
            <CloseIcon color="primary" />
          </IconButton>
        </Box>

        <Typography
          align="center"
          id="modal-modal-title"
          variant="h6"
          component="h2"
        >
          Добавить новую оствновку
        </Typography>
        <PointForm method="POST" values={values} setValues={setValues} />
      </Box>
    </Dialog>
  );
};
