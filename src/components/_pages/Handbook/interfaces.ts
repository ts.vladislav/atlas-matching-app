export type Order = "asc" | "desc";

export interface ICheckAllStatus {
  checked: boolean;
  indeterminate: boolean;
}
