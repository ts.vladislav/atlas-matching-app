import { ICheckAllStatus, Order } from "../interfaces";

export interface ITableHandbookHeadCell {
  checkAllStatus: ICheckAllStatus;
  onRequestSort: (event: React.MouseEvent<unknown>, property: string) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
}
