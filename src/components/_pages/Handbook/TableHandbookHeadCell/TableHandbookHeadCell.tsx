import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Checkbox from "@mui/material/Checkbox";
import TableSortLabel from "@mui/material/TableSortLabel";
import FilterListIcon from "@mui/icons-material/FilterList";
import Box from "@mui/material/Box";
import { visuallyHidden } from "@mui/utils";

import { headCells } from "../helpers/headCells";

import { ITableHandbookHeadCell } from "./interfaces";

export const TableHandbookHeadCell = (props: ITableHandbookHeadCell) => {
  const { onSelectAllClick, order, orderBy, onRequestSort, checkAllStatus } =
    props;
  const createSortHandler =
    (property: string) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={checkAllStatus?.indeterminate ?? false}
            checked={checkAllStatus?.checked ?? false}
            onChange={onSelectAllClick}
            inputProps={{
              "aria-label": "select all points",
            }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={"left"}
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
              sx={{ width: "100%" }}
              IconComponent={() => (
                <FilterListIcon
                  color={orderBy === headCell.id ? "primary" : "inherit"}
                  sx={{ opacity: orderBy === headCell.id ? 1 : 0.7 }}
                />
              )}
            >
              <Box
                component={"span"}
                sx={{
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                {headCell.label}
              </Box>

              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};
