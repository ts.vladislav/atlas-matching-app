import { IStopItemList } from "src/store/handbook/interfaces";

interface HeadCell {
  disablePadding: boolean;
  id: keyof IStopItemList;
  label: string;
  numeric: boolean;
}

export const headCells: readonly HeadCell[] = [
  {
    id: "id",
    numeric: true,
    disablePadding: false,
    label: "id",
  },
  {
    id: "title",
    numeric: true,
    disablePadding: false,
    label: "Наименование остановки",
  },
  {
    id: "latitude",
    numeric: true,
    disablePadding: false,
    label: "Широта",
  },
  {
    id: "longitude",
    numeric: true,
    disablePadding: false,
    label: "Долгота",
  },
];
