import { ICheckAllStatus } from "src/components/_pages/Handbook/interfaces";

import { ITableCheckboxAllStatus } from "./interfaces";

export const tableCheckboxAllStatus = (props: ITableCheckboxAllStatus) => {
  const { currentRows, selected } = props;
  const checkedRowsInPage = currentRows.filter((x) => selected.includes(x));

  const currentRowsCount = currentRows.length;
  const checkedRowsCount = checkedRowsInPage.length;

  const isChecked =
    currentRowsCount > 0 && currentRowsCount === checkedRowsCount;

  const isIdeterminate =
    checkedRowsCount > 0 && checkedRowsCount < currentRowsCount;

  const status: ICheckAllStatus = {
    checked: isChecked,
    indeterminate: isIdeterminate,
  };

  return status;
};
