export interface ITableCheckboxAllStatus {
  currentRows: readonly number[];
  selected: readonly number[];
}
