export interface ITableCheckboxAllStatus {
  currentRows: readonly number[];
  selected: readonly number[];
}

export interface ICheckAllStatus {
  checked: boolean;
  indeterminate: boolean;
}
