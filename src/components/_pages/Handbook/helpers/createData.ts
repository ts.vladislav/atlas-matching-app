import { IStopItemList } from "src/store/handbook/interfaces";

export default function createData(
  id: number,
  title: string,
  latitude: number,
  longitude: number,
): IStopItemList {
  return {
    id,
    title,
    latitude,
    longitude,
  };
}
