import React from "react";
import { Box, Typography, useTheme } from "@mui/material";

import { ButtonAtlasContained } from "src/components/ButtonAtlas";
import { TableModalAddNewPoint } from "src/components/_pages/Handbook/TableModalAddNewPoint";
import { useHandbook } from "src/store/handbook/handbook";

export const TableHandbookHeader = () => {
  const theme = useTheme();
  const selectedItemsCount = useHandbook((store) => store.selectedItems.length);

  const [open, setOpen] = React.useState<boolean>(false);
  const handleOpen = () => setOpen(true);

  return (
    <>
      <Box
        sx={{
          bgcolor: theme.atlasColors.white,
          padding: "34px 24px",
          borderRadius: "4px 4px 0px 0px",
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box>
          <Typography sx={{ fontWeight: "500" }} variant="h5">
            Справочник
          </Typography>
          <Typography variant="caption">
            Выбрано: {selectedItemsCount}
          </Typography>
        </Box>
        <Box>
          <ButtonAtlasContained onClick={handleOpen}>
            Добавить
          </ButtonAtlasContained>
        </Box>
      </Box>
      <TableModalAddNewPoint open={open} setOpen={setOpen} />
    </>
  );
};
