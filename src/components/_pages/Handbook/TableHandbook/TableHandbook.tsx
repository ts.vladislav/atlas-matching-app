import { IStopItemList } from "src/store/handbook/interfaces";

import { TableHandbookBody } from "../index";
import { TableHandbookHeader } from "../index";

import createData from "../helpers/createData";

// Temp function
const generateData = (count: number) => {
  const result = [];
  for (let i = 0; i < count; i++) {
    result.push(
      createData(
        100000 + i,
        "CityName",
        23.53453453345 + i,
        54.23423423424 - i,
      ),
    );
  }
  return result;
};

const rows: IStopItemList[] = generateData(3000);

export const TableHandbook = () => {
  return (
    <>
      <TableHandbookHeader />
      <TableHandbookBody rows={rows} />
    </>
  );
};
