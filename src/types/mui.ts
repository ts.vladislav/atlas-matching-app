declare module "@mui/material/styles" {
  interface Theme {
    atlasColors: {
      primary: string;
      second: string;
      grey500: string;
      grey400: string;
      grey300: string;
      grey200: string;
      grey100: string;
      grey50: string;
      white: string;
      black: string;
      red: string;
      green: string;
      greenLight: string;
      deepCold: string;
    };
  }
  // allow configuration using `createTheme`
  interface ThemeOptions {
    atlasColors?: {
      primary: string;
      second: string;
      grey500: string;
      grey400: string;
      grey300: string;
      grey200: string;
      grey100: string;
      grey50: string;
      white: string;
      black: string;
      red: string;
      green: string;
      greenLight: string;
      deepCold: string;
    };
  }
}

export {};
