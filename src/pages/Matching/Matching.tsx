import React from "react";
import classNames from "classnames/bind";

import { Container, Grid } from "@mui/material";
import { SelectChangeEvent } from "@mui/material/Select";

import {
  MatchingSearchModal,
  MatchingSelect,
  MatchingStickyBar,
} from "src/components/_pages/Matching";

import styles from "./styles.module.scss";

export const Matching = () => {
  const cn = classNames.bind(styles);

  const [personName, setPersonName] = React.useState<string[]>([]);

  const handleChangeSelect = (event: SelectChangeEvent<string | string[]>) => {
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value,
    );
  };

  const names: string[] = [
    "ИП Петров",
    "ООО Моторс",
    "ООО Мабико",
    "ООО Радуга дорог",
  ];

  const [open, setOpen] = React.useState<boolean>(false);
  const handleOpen = () => setOpen(true);

  return (
    <article className={cn(["article"])}>
      <Container>
        <Grid container spacing={2}>
          <Grid item xs={12} md={4}>
            <MatchingSelect
              names={names}
              personName={personName}
              handleChangeSelect={handleChangeSelect}
            />
          </Grid>
          <Grid item md={4}>
            <button onClick={handleOpen}>Show modal</button>
            <MatchingSearchModal open={open} setOpen={setOpen} />
          </Grid>
        </Grid>
      </Container>
      <MatchingStickyBar />
    </article>
  );
};
