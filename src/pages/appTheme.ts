import { createTheme } from "@mui/material/styles";
import { ruRU } from "@mui/material/locale";

export let appTheme = createTheme({});
appTheme = createTheme(
  {
    palette: {
      primary: {
        main: "#1673d6",
        contrastText: "#ffffff",
      },
    },
    atlasColors: {
      primary: "#1673d6",
      second: "#3e9bfe",
      grey500: "#68787D",
      grey400: "#A1ADB3",
      grey300: "#d8d8d8",
      grey200: "#E2E6EA",
      grey100: "#f2f3f5",
      grey50: "#f5f6f7",
      white: "#fff",
      black: "#000",
      red: "ff2d2d",
      green: "#00a066",
      greenLight: "#00b674",
      deepCold: "#f0f3f5",
    },
    components: {
      MuiContainer: {
        styleOverrides: {
          maxWidthLg: {
            [appTheme.breakpoints.up("lg")]: {
              maxWidth: "1232px",
            },
          },
        },
      },
    },
  },
  ruRU,
);
