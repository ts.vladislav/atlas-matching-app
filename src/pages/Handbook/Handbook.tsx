import { Container, Grid } from "@mui/material";
import classNames from "classnames/bind";
import { TableHandbook } from "src/components/_pages/Handbook";

import styles from "./styles.module.scss";

export const Handbook = () => {
  const cn = classNames.bind(styles);
  return (
    <article className={cn(["article"])}>
      <Container>
        <Grid container spacing={2}>
          <Grid item md={12}>
            <TableHandbook />
          </Grid>
        </Grid>
      </Container>
    </article>
  );
};
