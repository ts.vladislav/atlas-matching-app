import { Suspense, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";

import { Header, Footer } from "src/components";

import { RouteList } from "src/route";
import { appTheme } from "src/pages/appTheme";

const App = () => {
  useEffect(() => {
    // console.log(process.env.REACT_APP_ENV);
  }, []);
  return (
    <Router>
      <ThemeProvider theme={appTheme}>
        <div className="wrapper">
          <Header />
          <main className="content">
            <Suspense>
              <RouteList />
            </Suspense>
          </main>
          <Footer />
        </div>
      </ThemeProvider>
    </Router>
  );
};

export default App;
