type UniqueId = number;
type StopBusName = string;

export interface IHandbook {
  stopList: IStopItem[] | null;
  setStopList: (value: IStopItem[]) => void;
  handbook: boolean;
  selectedItems: readonly number[];
  setSelectedItems: (value: readonly number[]) => void;
}

export interface IStopItemList extends IStopItem {
  id: UniqueId;
}

export interface IStopItem {
  title: StopBusName;
  latitude: number; // Широта
  longitude: number; // Долгота
}
