import create from "zustand";

import { IHandbook } from "./interfaces";

export const useHandbook = create<IHandbook>((set) => ({
  stopList: null,
  setStopList: (value) => set({ stopList: value }),
  handbook: false,
  selectedItems: [],
  setSelectedItems: (value) => set({ selectedItems: value }),
}));
